#include <stdio.h>
#include <assert.h>
#include "mem.h"
#include "mem_internals.h"

#define TEST_FAILED 0
#define TEST_PASSED 1


static int test1(void){
    printf("\nTest 1 (Allocation memory and free it):");
    printf("\nAllocation...");
    void* block = _malloc(100);
    printf("\n");
    debug_heap(stdout, HEAP_START);
    if(!block){
        perror("Error: allocation didn't succeed");
        _free(block);
        return TEST_FAILED;
    }
    printf("Cleaning...");
    _free(block);
    printf("\n");
    debug_heap(stdout, HEAP_START);
    printf("Success!");
    return TEST_PASSED;
}


static int test2(void){
    printf("\nTest 2 (Free 1 block while couple are existing):");
    printf("\nAllocation...");
    void* block1 = _malloc(25);
    void* block2 = _malloc(70);
    printf("\n");
    debug_heap(stdout, HEAP_START);
    if(!block1 || !block2){
        perror("Error: allocation didn't succeed");
        _free(block1);
        _free(block2);
        return TEST_FAILED;
    }
    printf("Cleaning the second block(70 capacity)...");
    _free(block2);
    printf("\n");
    debug_heap(stdout, HEAP_START);
    printf("Success!");
    _free(block1);
    return TEST_PASSED;
}


static int test3(void){
    printf("\nTest 3 (Free 2 blocks while several are existing):");
    printf("\nAllocation...");
    void* block1 = _malloc(44);
    void* block2 = _malloc(34);
    void* block3 = _malloc(163);
    void* block4 = _malloc(87);
    void* block5 = _malloc(56);
    void* block6 = _malloc(97);
    printf("\n");
    debug_heap(stdout, HEAP_START);
    if(!block1 || !block2 || !block3 || !block4 || !block5 || !block6){
        perror("Error: allocation didn't succeed");
        _free(block1);
        _free(block2);
        _free(block3);
        _free(block4);
        _free(block5);
        _free(block6);
        return TEST_FAILED;
    }

    printf("Cleaning the third block(163 capacity)...");
    _free(block3);
    printf("\n");
    debug_heap(stdout, HEAP_START);

    printf("Cleaning the 5th block(56 capacity)...");
    _free(block5);
    printf("\n");
    debug_heap(stdout, HEAP_START);
    printf("Success!");

    _free(block1);
    _free(block2);
    _free(block4);
    _free(block6);
    return TEST_PASSED;
}


static int test4(void){
    printf("\nTest 4 (The memory is out, allocate new region):");
    printf("\nAllocation the first one...");
    printf("\n");
    void* block1 = _malloc(8000);
    debug_heap(stdout, HEAP_START);
    printf("\nAllocation the second...");
    printf("\n");
    void* block2 = _malloc(2000);
    debug_heap(stdout, HEAP_START);
    if(!block1 || !block2){
        perror("Error: allocation didn't succeed");
        _free(block1);
        _free(block2);
        return TEST_FAILED;
    }
    _free(block1);
    _free(block2);
    return TEST_PASSED;
}


static int test5(void){
    printf("\nTest 5 (Allocating the block bigger than heap):");
    printf("\nAllocation the first one...");
    printf("\n");
    void* block1 = _malloc(10000);
    debug_heap(stdout, HEAP_START);
    printf("\nAllocation the second...");
    printf("\n");
    void* block2 = _malloc(2000);
    debug_heap(stdout, HEAP_START);
    if(!block1 || !block2){
        perror("Error: allocation didn't succeed");
        _free(block1);
        _free(block2);
        return TEST_FAILED;
    }
    _free(block1);
    _free(block2);
    return TEST_PASSED;
}


void start_tests(){
    int points = 0;
    heap_init(REGION_MIN_SIZE);
    points += test1();
    printf("\n\n\n");
    points += test2();
    printf("\n\n\n");
    points += test3();
    printf("\n\n\n");
    points += test4();
    printf("\n\n\n");
    points += test5();
    printf("\n\nWe passed %d/5 tests\n", points);
}

